A small environment allowing confined execution of a particular NPM package.

Installation
------------

```
$ mkdir -p "$HOME/opt"
$ cd "$HOME/opt"
$ git clone https://gitlab.com/samueldr/diagrams-opt.git diagrams
$ cd "diagrams"
$ npm install
```

Usage
-----

```
$ ~/opt/diagrams/bin/diagrams --help
```

It could also be aliased in your shell, depending on your actual shell.

```
$ alias diagrams=~/opt/diagrams/bin/diagrams
$ diagrams --help
```

*See documentation about `.bashrc`, `.zshrc` or `config.fish`*
